import $ from 'jquery'
import jQuery from 'jquery'

/*!
 * @preserve
 * jquery.scrolldepth.js | v1.0
 * Copyright (c) 2016 Rob Flaherty (@robflaherty)
 * Licensed under the MIT and GPL licenses.
 */
!function (e) {
    "function"==typeof define&&define.amd?define(["jquery"],e):"object"==typeof module&&module.exports?module.exports=e(require("jquery")):e(jQuery)}(function (e) {
    "use strict";var n,t,r,o,i={minHeight:0,elements:[],percentage:!0,userTiming:!0,pixelDepth:!0,nonInteraction:!0,gaGlobal:!1,gtmOverride:!1,trackerName:!1,dataLayer:"dataLayer"},a=e(window),l=[],c=!1,u=0;return e.scrollDepth=function (p) {
        function s(e,i,a,l)
        {
            var c=p.trackerName?p.trackerName+".send":"send";o?(o({event:"ScrollDistance",eventCategory:"Scroll Depth",eventAction:e,eventLabel:i,eventValue:1,eventNonInteraction:p.nonInteraction}),p.pixelDepth&&arguments.length>2&&a>u&&(u=a,o({event:"ScrollDistance",eventCategory:"Scroll Depth",eventAction:"Pixel Depth",eventLabel:d(a),eventValue:1,eventNonInteraction:p.nonInteraction})),p.userTiming&&arguments.length>3&&o({event:"ScrollTiming",eventCategory:"Scroll Depth",eventAction:e,eventLabel:i,eventTiming:l})):(n&&(window[r](c,"event","Scroll Depth",e,i,1,{nonInteraction:p.nonInteraction}),p.pixelDepth&&arguments.length>2&&a>u&&(u=a,window[r](c,"event","Scroll Depth","Pixel Depth",d(a),1,{nonInteraction:p.nonInteraction})),p.userTiming&&arguments.length>3&&window[r](c,"timing","Scroll Depth",e,l,i)),t&&(_gaq.push(["_trackEvent","Scroll Depth",e,i,1,p.nonInteraction]),p.pixelDepth&&arguments.length>2&&a>u&&(u=a,_gaq.push(["_trackEvent","Scroll Depth","Pixel Depth",d(a),1,p.nonInteraction])),p.userTiming&&arguments.length>3&&_gaq.push(["_trackTiming","Scroll Depth",e,l,i,100])))}function h(e)
        {
            return{"25%":parseInt(.25*e,10),"50%":parseInt(.5*e,10),"75%":parseInt(.75*e,10),"100%":e-5}}function g(n,t,r)
        {
            e.each(
                n,function (n,o) {
                    -1===e.inArray(n,l)&&t>=o&&(s("Percentage",n,t,r),l.push(n))}
            )}function f(n,t,r)
        {
            e.each(
                n,function (n,o) {
                    -1===e.inArray(o,l)&&e(o).length&&t>=e(o).offset().top&&(s("Elements",o,t,r),l.push(o))}
            )}function d(e)
        {
            return(250*Math.floor(e/250)).toString()}function m()
        {
            y()}function v(e,n)
        {
            var t,r,o,i=null,a=0,l=function () {
                a=new Date,i=null,o=e.apply(t,r)};return function () {
            var c=new Date;a||(a=c);var u=n-(c-a);return t=this,r=arguments,0>=u?(clearTimeout(i),i=null,a=c,o=e.apply(t,r)):i||(i=setTimeout(l,u)),o}}function y()
        {
            c=!0,a.on(
                "scroll.scrollDepth",v(
                    function () {
                        var n=e(document).height(),t=window.innerHeight?window.innerHeight:a.height(),r=a.scrollTop()+t,o=h(n),i=+new Date-D;return l.length>=p.elements.length+(p.percentage?4:0)?(a.off("scroll.scrollDepth"),void(c=!1)):(p.elements&&f(p.elements,r,i),void(p.percentage&&g(o,r,i)))},500
                )
            )}var D=+new Date;p=e.extend({},i,p),e(document).height()<p.minHeight||(p.gaGlobal?(n=!0,r=p.gaGlobal):"function"==typeof ga?(n=!0,r="ga"):"function"==typeof __gaTracker&&(n=!0,r="__gaTracker"),"undefined"!=typeof _gaq&&"function"==typeof _gaq.push&&(t=!0),"function"==typeof p.eventHandler?o=p.eventHandler:"undefined"==typeof window[p.dataLayer]||"function"!=typeof window[p.dataLayer].push||p.gtmOverride||(o=function (e) {
            window[p.dataLayer].push(e)}),e.scrollDepth.reset=function () {
            l=[],u=0,a.off("scroll.scrollDepth"),y()},e.scrollDepth.addElements=function (n) {
            "undefined"!=typeof n&&e.isArray(n)&&(e.merge(p.elements,n),c||y())},e.scrollDepth.removeElements=function (n) {
            "undefined"!=typeof n&&e.isArray(n)&&e.each(
                n,function (n,t) {
                    var r=e.inArray(t,p.elements),o=e.inArray(t,l);-1!=r&&p.elements.splice(r,1),-1!=o&&l.splice(o,1)}
            )},m())},e.scrollDepth});


function isGtag()
{
    return typeof gtag !== "undefined";
}

function isGa()
{
    return typeof gtag === "undefined";
}

/*
* ScrollAnalytics 1.0
*
* Óscar Jiménez
* Copyright 2018, MIT License
*
*/

class ScrollAnalytics {
    constructor(options) {
        var _this = this;
        this.userTiming = false;
        this.elements = ['[data-scroll-tracker]'];
        this.metric = isGa() ? "metric1" : "scrollDepth";
        this.debug = false;
        this.trackers = [];
        this.lastPercent = 25;
        this.scrollDepth = null;

        this.validate();

        this.eventHandler = function (data) {
            if (data.eventAction === "Percentage"
                && data.eventCategory === "Scroll Depth") {
                _this.lastPercent = Number(data.eventLabel.replace("%", ""));

                if (_this.debug === true) {
                    console.log(data);
                }
            }
        };

        if (typeof options !== 'undefined') {
            $.extend(this, options);
        }

        _this.bindEvents();
    }
    validate() {
        if (isGtag() === "undefined" && isGa() === "undefined") {
            throw "ga or gat must be defined";
        }

        if (typeof jQuery === "undefined") {
            throw "jquery must be defined";
        }
    }
    bindEvents() {
        $.scrollDepth(
            {
                elements: this.elements,
                userTiming: this.userTiming,
                eventHandler: this.eventHandler
            }
        );

        var _this = this;

        $(window).on(
            "beforeunload", function () {
                _this.send();
            }
        );
    }
    send() {
        var _this = this;
        var metric = _this.metric;
        var value = _this.lastPercent;
        var label = "Scroll depth";
        var json = {};

        json[metric] = value;

        if (_this.debug === false) {
            if (isGa()) {
                ga("send", "event", "scroll depth", "scroll", label, json);

                $.each(
                    _this.trackers, function (k, v) {
                        ga(v + ".send", "event", "scroll depth", "scroll", label, json);
                    }
                );
            }

            if (isGtag()) {

                json['event_category'] = 'scroll depth';
                json['event_label'] = label;

                gtag('event', "scroll", json);

                $.each(
                    _this.trackers, function (k, v) {
                        json["send_to"] = v;
                        gtag('event', "scroll", json);
                    }
                );
            }
        }

        if (_this.debug === true) {
            if (isGa()) {
                console.log("send", "event", "scroll depth", "scroll", label, json);

                $.each(
                    _this.trackers, function (k, v) {
                        console.log(v + ".send", "event", "scroll depth", "scroll", label, json);
                    }
                );

            }

            if (isGtag()) {

                json['event_category'] = 'scroll depth';
                json['event_label'] = label;

                console.log('event', "scroll depth", json);

                $.each(
                    _this.trackers, function (k, v) {
                        json["send_to"] = v;
                        console.log('event', "scroll depth", json);
                    }
                );
            }
        }
    }
    sendAndUpdate() {
        this.send();
        this.update();
    }
    update() {
        $.scrollDepth.reset();
        this.lastPercent = 25;
    }
}

export default ScrollAnalytics