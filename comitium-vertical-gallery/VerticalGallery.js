import $ from 'jquery'

var VerticalGallery = function (element) {
    var _images = [];
    var loading = false;
    var originalData = {
        'title': $('title').text(),
        'description': $('meta[property="og:description"]').attr('content'),
        'url': $('meta[property="og:url"]').attr('content'),
        'image': {
            'url': $('meta[property="og:image"]').attr('content'),
            'width': $('meta[property="og:image:width"]').attr('content'),
            'height': $('meta[property="og:image:height"]').attr('content'),
        }
    };
    var currentData = JSON.stringify(originalData);

    var galleryAsset = $(element).find('[data-selected="true"]');

    if (galleryAsset.length > 0) {
        setTimeout(function () {
            $('html,body').animate({scrollTop: $(galleryAsset).offset().top}, 400);
        }, 500)
    }

    function onVisualizeImage($image, currentData) {
        var head = $('head');
        var figureParent = $image.closest("div[data-id='c-gallery__fig']");
        var h3Element = $(figureParent).find('[data-role="title"]');
        var title = h3Element.length > 0 ? h3Element.text() : $('title', head).text();
        var pElement = $(figureParent).find('[data-role="description"]');
        var description = pElement.length > 0 ? pElement.text() : $('meta[name="description"]', head).attr('content');
        var newLink = $(figureParent).data('link');

        var newData = {
            'title': title,
            'description': description,
            'url': newLink,
            'image': {
                'url': $image.attr('src'),
                'width': $image.attr('width'),
                'height': $image.attr('height'),
            }
        };

        var newJsonData = JSON.stringify(newData);

        if (newJsonData != currentData) {

            $('title', head).text(title);
            $('meta[property="og:title"]', head).attr('content', title);
            $('meta[name="title"]', head).attr('content', title);
            $('meta[name="description"]', head).attr('content', description);
            $('meta[property="og:description"]', head).attr('content', description);
            $('meta[name="twitter:description"]', head).attr('content', description);
            $('meta[name="twitter:image:src"]', head).attr('content', $image.attr('src'));
            $('meta[property="og:image"]', head).attr('content', $image.attr('src'));
            $('meta[property="og:image:width"]', head).attr('content', $image.attr("width"));
            $('meta[property="og:image:height"]', head).attr('content', $image.attr("height"));
            $('meta[name="twitter:url"]', head).attr('content', newLink);
            $('meta[property="og:url"]', head).attr('content', newLink);

            if (typeof (history.replaceState) !== "undefined") {
                history.replaceState({}, title, newLink);
            }

            sendPageviews();

            currentData = newJsonData;
        }

    }

    $(window).scroll(function () {
        if (!loading) {
            loading = true;

            var _windowScroll = $(window).scrollTop();

            var _imageIndex = 0;
            var _imagesLength = _images.length;

            $('[data-role="gallery-image"]', element).each(function () {
                var _this = $(this);
                var _offset = _this.offset().top;
                var _height = _this.height();
                var _scroll = _offset - (_height / 4);

                if (_windowScroll > _scroll) {
                    if (_imagesLength === 0) {
                        _images.push({"visualized": true});
                        _imagesLength = _images.length;
                        var _image = _this.find("img");
                        onVisualizeImage(_image, currentData);
                    } else {
                        if (_images[_imageIndex] === undefined) {
                            _images.push({"visualized": true});
                            _imagesLength = _images.length;
                            var _image = _this.find("img");
                            onVisualizeImage(_image, currentData);
                        }
                    }
                }
                _imageIndex++;
            });
            loading = false;


            if(!loadingBanner) {
                var loadingBanner = true;

                $('[data-role="banner"]', element).each(function () {
                    var _this = $(this);
                    var _offset = _this.offset().top;
                    var _height = _this.height();
                    var _scroll = _offset - (_height / 4);

                    var _windowBottom = _windowScroll + window.innerHeight;

                    if (_windowBottom > _scroll) {
                        onVisualizeBanner(_this);
                    }
                });

                loadingBanner = false;
            }
        }
    });
};

function sendPageviews() {
    // Stats
    if (typeof gtag !== "undefined") {
        gtag('event', 'page_view', {
            title: document.title,
            location: location.href,
            page: location.pathname
        });
    } else if (typeof ga !== "undefined") {
        ga("send", {
            hitType: "pageview",
            title: document.title,
            location: location.href,
            page: location.pathname
        })
    }

    if(typeof COMSCORE !== 'undefined') {
        COMSCORE.beacon({c1: "2", c2: "21043127"});
        $.get("https://www.elnacional.cat/_comscore/page-candidate");
    }
}

function createBanner(replaceElement) {
    var tagId = replaceElement.data("banner-tag-id");
    var siteId = replaceElement.data("banner-siteid");
    var pageId = replaceElement.data("banner-pageid");
    var format = replaceElement.data("banner-format");
    // var pgName = replaceElement.data("banner-page-name");

    var bannerContainer = document.createElement("div");
    bannerContainer.setAttribute('data-banner-tag', tagId);

    var smartDiv = document.createElement("div");
    smartDiv.id = tagId;
    var smartTag = document.createElement("script");

    if (siteId !== "" && pageId != "" && format != "") {
        $(smartTag).text("sas.call('std', {siteId:" + siteId + ",pageId:" + pageId + ",formatId:" + format + ",target: ($('.head-lang > a').hasClass('cat') ? 'idioma=es;' : 'idioma=ca;'),tagId:" + tagId + "});");
    }
    bannerContainer.appendChild(smartDiv);
    bannerContainer.appendChild(smartTag);

    return bannerContainer;
}

function onVisualizeBanner(element) {
    var banner = createBanner(element);
    element.replaceWith($(banner));
}


export default VerticalGallery