import LazyLoad from "vanilla-lazyload";
import Picturefill from "picturefill";
import ImageResizer from "comitium-image-resize";


const ImageProcess = {};
ImageProcess.init = () => {
    window.lazyload = new LazyLoad();
    window.pictureFill = new Picturefill();
    window.ImageResizer = new ImageResizer();
}
export default ImageProcess