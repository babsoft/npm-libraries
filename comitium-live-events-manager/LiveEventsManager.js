import $ from 'jquery'
import moment from 'moment'
class LiveEventsManager {




    constructor() {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }

        this.TEMPLATE_TITLE_PLACEHOLDER = '__TITLE__';
        this.TEMPLATE_ID_PLACEHOLDER = '__ID__';
        this.TEMPLATE_HOUR_PLACEHOLDER = '__HOUR__';
        this.TEMPLATE_DESCRIPTION_PLACEHOLDER = '__DESCRIPTION__';
        this.TEMPLATE_TITLE_SHORT_PLACEHOLDER = '__TITLE_SHORT__';
        this.TEMPLATE_DATE_PLACEHOLDER = '__DATE__';
        this.TEMPLATE_SHORT_URL_PLACEHOLDER ='__URL_SHORT__';
        this.LOADING_CLASS = 'loading';
        this.TYPOLOGY_SPORTS ='sports';
        this.TYPOLOGY_GENERAL = 'general';
        this.liveEventTypeData = 'live-event-type';
        this.intervalSecondsData = 'live-event-interval-seconds';
        this.jsonUrlData = 'live-event-json-url';
        this.pageUrlData = 'live-event-page-url';
        this.objectIdData = 'live-event-object-id';
        this.containerSelector = '#live-events-container';
        this.notificationSoundData = 'live-event-notification-sound';
        this.updatedAtSelector = '#live-event-update-at';
        this.orderData = 'order';
        this.orderSelectedData = 'order-selected';
        this.scoreBoardSelector = '#live-event-scoreboard';
        this.scoreBoardUpdateData = 'url-update';
        this.liveEventTypologyData = 'live-event-typology';
        this.isCommentData = 'live-event-is-comment';
        this.comscoreUrlData = 'live-event-comscore-url';
        this.postTemplateData = 'post-template';
        this.scoreBoardTemplateData = 'scoreboard-template';
        this.webSocketAppKeyData = 'app-key';
        this.localeData = 'live-event-locale';

        this.interval = null;
        this.intervalSeconds = null;
        this.type = null;
        this.jsonUrl = null;
        this.pageUrl = null;
        this.objectId = null;
        this.lastTimeStamp = null;
        this.commentContainer = $(this.containerSelector);
        this.notificationSound = null;
        this.order = 'desc';
        this.scoreBoardUpdateUrl = null;
        this.typology = null;
        this.currentUrl = window.location.origin + window.location.pathname;
        this.comscorePageViewCandidateUrl = null;
        this.postTemplate = '';
        this.scoreBoardTemplate = '';
        this.webSocketAppKey = null;
        this.locale = null;
    }
    init() {
        var isComment = $('[data-' + this.isCommentData + ']').data(this.isCommentData);
        this.type = $('[data-' + this.liveEventTypeData + ']').data(this.liveEventTypeData);
        this.objectId = $('[data-' + this.objectIdData + ']').data(this.objectIdData);
        this.pageUrl = $('[data-' + this.pageUrlData + ']').data(this.pageUrlData);
        this.typology = $('[data-' + this.liveEventTypologyData + ']').data(this.liveEventTypologyData);
        this.intervalSeconds = parseInt($('[data-' + this.intervalSecondsData + ']').data(this.intervalSecondsData));
        this.comscorePageViewCandidateUrl = $('[data-' + this.comscoreUrlData + ']').data(this.comscoreUrlData);
        this.postTemplate = $('[data-' + this.postTemplateData + ']').data(this.postTemplate);
        this.scoreBoardTemplate = $('[data-' + this.scoreBoardTemplateData + ']').data(this.scoreBoardTemplate);
        this.webSocketAppKey = $('[data-' + this.webSocketAppKeyData + ']').data(this.webSocketAppKey);
        this.locale = $('[data-' + this.localeData + ']').data(this.locale);

        moment.locale(this.locale);

        if (isComment === 0) {
            this.initOrder();
            this.initAudioNotifications();
            this.initPaginator();

            if (this.type === 'open' && this.order === 'desc') {
                this.jsonUrl = $('[data-' + this.jsonUrlData + ']').data(this.jsonUrlData);

                if (this.jsonUrl) {
                    this.initInterval();
                }
            }
        }

        if (this.typology === 'sports' && this.type === 'open') {
            this.initScoreBoard();
        }

        this.cleanData();
        this.webSocketValidation();
    }
    cleanData() {
        $('[data-' + this.pageUrlData + ']').removeAttr('data-' + this.pageUrlData);
        $('[data-' + this.jsonUrlData + ']').removeAttr('data-' + this.jsonUrlData);
        $('[data-' + this.objectIdData + ']').removeAttr('data-' + this.objectIdData);
        $('[data-' + this.notificationSoundData + ']').removeAttr('data-' + this.notificationSoundData);
        $('[data-' + this.liveEventTypeData + ']').removeAttr('data-' + this.liveEventTypeData);
        $('[data-' + this.intervalSecondsData + ']').removeAttr('data-' + this.intervalSecondsData);
        $(this.scoreBoardSelector).removeAttr('data-' + this.scoreBoardUpdateData);
        $('[data-' + this.liveEventTypologyData + ']').removeAttr('data-' + this.liveEventTypologyData);
        $('[data-' + this.isCommentData + ']').removeAttr('data-' + this.isCommentData);
        $('[data-' + this.comscoreUrlData + ']').removeAttr('data-' + this.pageUrlData);
    }
    initInterval() {
        var $this = this;

        if ($this.mustToUsePostWebSocket() === true) {
            $this.initWebSocket();
        } else {
            $this.interval = setInterval(
                function () {
                    $this.update();
                }, $this.intervalSeconds
            );
        }
    }
    initPaginator() {

        var $this = this;
        $('#live-event-paginator').on(
            'click', function () {
                $(this).addClass(this.LOADING_CLASS);
                $this.paginate();
            }
        );
    }
    initOrder() {
        $('[data-' + this.orderData + ']').on(
            'click', function () {
                $('.selector strong').html(jQuery(this).html());
                $('.selector-options')
                    .removeClass('open')
                    .slideToggle(200);
                window.location = window.location.pathname + '?order=' + encodeURIComponent($(this).data(this.orderData));
            }
        );

        var el = $('[data-' + this.orderSelectedData + ']');

        if (el) {
            this.order = el.data(this.orderSelectedData);
        }
    }
    initAudioNotifications() {
        var audioUrl = $('[data-' + this.notificationSoundData + ']').data(this.notificationSoundData);

        if (audioUrl) {
            this.notificationSound = new Audio(audioUrl);
        }
    }
    paginate() {
        var $this = this;
        var lastDate = $('[data-date]', '#live-events-container').last().data('date');
        $.get(
            this.pageUrl, { lastDate: lastDate, liveEventId: $this.objectId, order: $this.order, currentUrl: $this.currentUrl }, function (data) {
                $this.renderComments(data.comments, true);
                if (!data.moreResults) {
                    $('#live-event-paginator').remove();
                }
            }
        ).always(
            function () {
                $('#live-event-paginator').removeClass(this.LOADING_CLASS);
            }
        );
    }
    update() {
        var $this = this;

        if (!$this.jsonUrl) {
            return false;
        }

        $this.doRequest(
            $this.jsonUrl, { liveEventId: $this.objectId, currentUrl: $this.currentUrl }, function (data) {
                if ($this.lastTimeStamp !== data.timestamp) {
                    var hasUpdate = $this.renderComments(data.comments);

                    $this.lastTimeStamp = data.timestamp;

                    if (hasUpdate) {
                        $this.notify();

                        $(window).trigger('cs:leftLiveEventBannerUpdatePosition');
                    }
                }
            }
        );
    }
    doRequest(url, params, callback) {
        params = typeof params === 'undefined' ? {} : params;
        callback = typeof callback === 'undefined' ? function () { } : callback;
        $.get(url, params, callback, 'json');
    }
    renderComments(data, pagination) {
        pagination = typeof pagination === 'undefined' ? false : pagination;

        var hasUpdate = false;

        if (data !== null && data.length > 0) {
            var length = data.length;

            for (var i = 0; i < length; i++) {
                var update = this._renderComment(data[i], pagination);
                if (update && !hasUpdate) {
                    hasUpdate = true;
                }
            }
        }

        return hasUpdate;
    }
    _renderComment(data, pagination) {
        var element = $('[data-id="' + data.id + '"]');
        var container = this.commentContainer,
            date = moment(data.publishDate.date, 'YYYY-MM-DD HH:mm:ss'),
            hour = this.typology === this.TYPOLOGY_SPORTS ? data.timeText : date.format('HH:mm');

        if (hour === null && date !== null && this.typology === this.TYPOLOGY_SPORTS) {
            hour = date.format('HH:mm');
        } else if (hour === null) {
            hour = '';
        }

        var update = false;

        var elementHtml = $(data.html);

        if (data.title === null || data.title === '') {
            elementHtml.find('h2').remove();
        }

        if (hour === '') {
            elementHtml.find('.info').remove();
        }

        if (typeof data.format !== 'undefined' && data.format !== null && data.format !== '') {
            elementHtml
                .find('.live-event-post')
                .addClass(data.format);
        }

        if (element.length === 0 && !pagination) {
            container.prepend(elementHtml);

            setTimeout(
                function () {
                    elementHtml
                        .find('.live-event-post')
                        .addClass('highlight');
                }, 200
            );

            setTimeout(
                function () {
                    elementHtml
                        .find('.live-event-post')
                        .removeClass('highlight');
                }, 3000
            );

            update = true;
        } else if (pagination) {
            container.append(elementHtml);
            /*} else if(data.updatedAt !== null) {
                element.closest('.row').replaceWith(elementHtml);*/
        }

        $(window).trigger('cs:leftLiveEventBannerLoad');

        return update;
    }
    notify() {
        if (typeof moment !== 'undefined') {
            var lastDate = $('[data-id]', this.commentContainer).first().data('date');
            var text = $(this.updatedAtSelector).html();

            $(this.updatedAtSelector).html(
                text.split(' ')[0] + ' ' + moment(lastDate, "YYYY-MM-DD hh:mm:ss").fromNow()
            );
        }

        if (typeof gtag !== 'undefined') {
            gtag('event', 'page_view', { 'send_to': 'UA-67391421-1' });
            gtag('event', 'page_view', { 'send_to': 'UA-127582693-1' });
        } else if (typeof ga !== 'undefined') {
            ga('send', { hitType: 'pageview', page: location.pathname });
        }

        if (typeof COMSCORE !== 'undefined' && this.comscorePageViewCandidateUrl !== null && typeof this.comscorePageViewCandidateUrl !== 'undefined' && this.comscorePageViewCandidateUrl !== '') {
            COMSCORE.beacon({ c1: "2", c2: "21043127" });
            $.get(this.comscorePageViewCandidateUrl /*+ '&t=' +  Date.now()*/);
        }

        if (this.notificationSound) {
            this.notificationSound.play();
        }
    }
    initScoreBoard() {
        var scoreBoardElement = $(this.scoreBoardSelector),
            $this = this;

        if (scoreBoardElement.length > 0) {
            $this.scoreBoardUpdateUrl = scoreBoardElement.data(this.scoreBoardUpdateData);

            if (!$this.scoreBoardUpdateUrl) {
                return false;
            }

            if (isNaN($this.intervalSeconds)) {
                $this.intervalSeconds = parseInt($('[data-' + this.intervalSecondsData + ']').data(this.intervalSecondsData));
            }

            if ($this.mustToUseScoreboardWebSocket() === false) {
                setInterval(
                    function () {
                        $this.updateScoreBoard();
                    }, $this.intervalSeconds
                );
            }
        }
    }
    updateScoreBoard() {
        var $this = this;
        $this.doRequest(
            $this.scoreBoardUpdateUrl, { liveEventId: $this.objectId }, function (data) {
                if (data.html) {
                    $(this.scoreBoardSelector).replaceWith(data.html);
                }
            }
        );
    }
    webSocketValidation() {
        if (this.postTemplate !== '' &&
            this.webSocketAppKey !== null &&
            typeof ComitiumLiveEventSocket === 'undefined') {
            console.info("ComitiumLiveEventSocket library must be load to use web sockets");
        }
    }
    initWebSocket() {
        var $this = this;

        ComitiumLiveEventSocket.init(this.webSocketAppKey, $this.objectId, {
            onCreate: function (data) { $this.webSocketOnCreate(data); },
            onUpdate: function (data) { $this.webSocketOnEdit(data); },
            onDelete: function (data) { $this.webSocketOnDelete(data); },
            onScoreboardUpdate: function (data) { $this.webSocketScoreBoardOnUpdate(data); },
            computePresence: true
        });
    }
    webSocketScoreBoardOnUpdate(data) {
        var mapObj = {
            "__scoreBoard.homeTeam__": data.homeTeam,
            "__scoreBoard.homeValue__": data.homeValue,
            "__scoreBoard.awayTeam__": data.awayTeam,
            "__scoreBoard.awayValue__": data.awayValue
        };

        var regex = new RegExp(Object.keys(mapObj).join("|"), "gi");

        var html = $(this.scoreBoardTemplate.replace(regex, function (matched) {
            return mapObj[matched];
        }));

        if (data.freeText) {
            for (var x = 0; x < html.length; x++) {
                var candidate = $(html[x]);

                if (candidate.attr('id') === 'scoreboard-text') {
                    html[x].innerHTML = candidate.attr('data-free-text-' + data.freeText);
                }
            }
        }

        $('#live-event-scoreboard').html(html);
    }
    isSport() {
        return this.typology === 'sports';
    }
    mustToUsePostWebSocket() {
        return (
            this.postTemplate !== '' &&
            this.webSocketAppKey !== null &&
            typeof ComitiumLiveEventSocket !== 'undefined'
        );
    }
    mustToUseScoreboardWebSocket() {
        return (
            this.scoreBoardTemplate !== '' &&
            this.webSocketAppKey !== null &&
            typeof ComitiumLiveEventSocket !== 'undefined'
        );
    }
    renderWebSocketPost(template, data) {
        var mapObj = {
            "__post.id__": data.id,
            "__post.title_h3__": data.title ? '<h3>' + data.title + '</h3>' : '',
            "__post.title__": data.title ? data.title : '',
            "__post.description__": data.description ? data.description : '',
            "__post.format__": String(data.format),
            "__post.publishDate__": moment(data.publishDate).format("Y-MM-DD HH:mm:ss"),
            "__post.publishDate_time__": this.isSport() ? (data.timeText ? data.timeText + "'" : '') : moment(data.publishDate).format("HH:mm"),
            "__post.publishDate_date__": this.isSport() ? '' : moment(data.publishDate).format("DD/MM/Y")
        };

        var regex = new RegExp(Object.keys(mapObj).join("|"), "gi");

        return template.replace(regex, function (matched) {
            return mapObj[matched];
        });
    }
    webSocketOnCreate(data) {

        if (data.locale !== this.locale) {
            return;
        }

        var post = Object.assign(data, { html: this.renderWebSocketPost(this.postTemplate, data) });

        this._renderComment(post);

        this.notify();

        $(window).trigger('cs:leftLiveEventBannerUpdatePosition');
    }
    webSocketOnEdit(data) {
        if (data.locale !== this.locale) {
            return;
        }

        var element = $('[data-id="' + data.id + '"]');

        if (element) {
            element.replaceWith(this.renderWebSocketPost(this.postTemplate, data));
        }
    }
    webSocketOnDelete(data) {
        if (data.locale !== this.locale) {
            return;
        }

        var element = $('[data-id="' + data.id + '"]');

        if (element) {
            element.remove();
        }
    }
}

export default LiveEventsManager