import $ from 'jquery'

/**
 * @param options
 * @constructor
 */
class ImageResizer {
    constructor(options) {
    }

    /**
     * Initialize
     */
    init() {
        this.resizeImages();
        this.bindGlobalEvents();
    }

    /**
     * Resize all page images
     */
    resizeImages() {
        var $this = this;
        $('img.cs-responsive-image').each(function () {
            $this.resizeImage(this);
        });
    }

    bindGlobalEvents() {
        $(window).on('cs:load-ajax', function (event, images) {
            setTimeout(function () {
                var elements = typeof images === 'undefined' ? $('img.cs-responsive-image') : images;

                elements.each(function () {
                    imageResizer.resizeImage(this);
                });
            }, 300);
        });
    }

    /**
     * Resize one image
     * @param img
     */
    resizeImage(img) {
        if (!!$(img).data('original')) {
            var finalSrc = $(img).data('original');
            if (!!$(img).attr('data-prosrcset')) {
                var checkSelector = $(img).data('check');
                var checkElement = $(img).closest(checkSelector);

                var srces_string = $(img).data("prosrcset");
                var aSrces = srces_string.split(",");
                var arrayFinalImage = aSrces[0].split(":");
                finalSrc = arrayFinalImage[1];
                if (aSrces.length > 0) {
                    var width = checkElement.length === 0 ? $(img).width() : $(img).closest(checkSelector).width();
                    var height = checkElement.length === 0 ? $(img).height() : $(img).closest(checkSelector).height();
                    for (var x = 0; x < aSrces.length; x++) {
                        var arraySrc = aSrces[x].split(":");
                        var arraySize = arraySrc[0].split("|");
                        if ((parseInt(arraySize[0]) >= width || parseInt(arraySize[1]) >= height) && arraySrc[1] != finalSrc) {
                            finalSrc = arraySrc[1];
                        }
                    }
                }
            }

            if (finalSrc.indexOf('http:') < 0
                && finalSrc.indexOf('https:') < 0) {
                finalSrc = location.protocol + finalSrc;
            }

            $(img).attr("src", finalSrc);
        }
    }
}

export default ImageResizer