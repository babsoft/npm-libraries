import $ from 'jquery'
import jQuery from 'jquery'

function isGtag()
{
    return typeof gtag !== "undefined";
}

function isGa()
{
    return typeof gtag === "undefined";
}

/*
   * SocialAnalytics 1.0
   *
   * Óscar Jiménez
   * Copyright 2018, MIT License
   *
   */

class SocialAnalytics {
    constructor(options) {
        this.facebookSelector = '.fb a';
        this.twitterSelector = '.tw a';
        this.googleSelector = '.google a';
        this.whatsappSelector = '.wa a';
        this.trackers = [];
        this.scope = 'body';
        this.faceboookMetric = isGa() ? 'metric2' : 'facebookClick';
        this.twitterMetric = isGa() ? 'metric3' : 'twitterClick';
        this.googleMetric = isGa() ? 'metric4' : 'googleClick';
        this.whatsappMetric = isGa() ? 'metric5' : 'whatsappClick';
        this.debug = false;

        this.validate();

        if (typeof options !== 'undefined') {
            $.extend(this, options);
        }

        this.bindEvents();
    }
    validate() {
        if (isGtag() === "undefined" && isGa() === "undefined") {
            throw "ga or gat must be defined";
        }

        if (typeof jQuery === "undefined") {
            throw "jQuery must be defined";
        }
    }
    bindEvents() {
        if (typeof this.facebookSelector !== "undefined") {
            this.bindClick(this.facebookSelector, this.faceboookMetric, 1, "Facebook click");
        }

        if (typeof this.twitterSelector !== "undefined") {
            this.bindClick(this.twitterSelector, this.twitterMetric, 1, "Twitter click");
        }

        if (typeof this.googleSelector !== "undefined") {
            this.bindClick(this.googleSelector, this.googleMetric, 1, "Google click");
        }

        if (typeof this.whatsappSelector !== "undefined") {
            this.bindClick(this.whatsappSelector, this.whatsappMetric, 1, "Whatsapp click");
        }
    }
    bindClick(selector, metric, value, label) {
        var _this = this;

        $(selector, this.scope).on(
            "click", function (e) {
                var json = {};
                json[metric] = value;

                if (_this.debug === false) {

                    if (isGa()) {
                        ga("send", "event", "social networks", "click", label, json);

                        $.each(
                            _this.otherTrackers, function (k, v) {
                                ga(v + ".send", "event", "social networks", "click", label, json);
                            }
                        );
                    }

                    if (isGtag()) {
                        json['event_category'] = 'social networks';
                        json['event_label'] = label;

                        gtag('event', "click", json);

                        $.each(
                            _this.trackers, function (k, v) {
                                json["send_to"] = v;
                                gtag('event', "click", json);
                            }
                        );

                    }
                }

                if (_this.debug === true) {
                    e.preventDefault();

                    if (isGa()) {
                        console.log("send", "event", "social networks", "click", label, json);
                    }

                    if (isGtag()) {
                        json['event_category'] = 'social networks';
                        json['event_label'] = label;

                        console.log('event', "click", json);

                        $.each(
                            _this.trackers, function (k, v) {
                                json["send_to"] = v;
                                console.log('event', "click", json);
                            }
                        );
                    }
                }
            }
        );
    }
    update() {
        this.bindEvents();
    }
}




export default SocialAnalytics
